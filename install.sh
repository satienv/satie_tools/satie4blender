#!/bin/bash
version=`blender --version | cut -d ' ' -f 2 | sed 's/\..$//'`
mkdir -p ~/.config/blender/$version/scripts/addons/satie
cp -v satie/* ~/.config/blender/$version/scripts/addons/satie
