# Print core satie class debug messages
DEBUG_SATIE = False

# Print core satie class' run loop debug messages
DEBUG_SATIE_LOOP = False

# Print OSC debug messages
DEBUG_OSC = False

# Print OSC's run loop debug messages
DEBUG_OSC_LOOP = False

# Print synth debug messages
DEBUG_SYNTH = False

# Print synth's run loop debug messages
DEBUG_SYNTH_LOOP = False

# Print plugin debug messages
DEBUG_PLUGIN = False
