import bpy
from bpy.types import Operator


class Activate(Operator):
    bl_idname = "satie.activate"
    bl_label = "Activate SATIE"
    bl_description = "Activate SATIE (Save user preferences to enable auto-connect)"

    def execute(self, context):
        bpy.context.preferences.addons[__package__.split('.')[0]].preferences.active = True
        return {"FINISHED"}
