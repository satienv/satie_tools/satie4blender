import bpy
from bpy.types import Operator


class Configure(Operator):
    bl_idname = "satie.configure"
    bl_label = "Configure"
    bl_description = "Configure SATIE in the user preferences window"

    def execute(self, context):
        bpy.ops.preferences.addon_show(module="satie")
        return {'FINISHED'}
