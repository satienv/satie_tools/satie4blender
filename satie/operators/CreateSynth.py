import bpy
from bpy.props import BoolProperty, FloatProperty, IntProperty, PointerProperty, StringProperty
from bpy.types import Operator
from typing import TYPE_CHECKING, Dict, List, Optional
from ..Properties import SatieObjectProperties, SatieSynthProperty

if TYPE_CHECKING:
    from pysatie import Node, Plugin, Satie


def fill_properties(context: bpy.context) -> Optional[PointerProperty]:
    props_dict: Dict = {}
    prop_list: List = []
    obj: bpy.Object = context.object
    satie: Satie = bpy.satie
    synth: Node = satie.get_node_by_name(obj.name)
    if synth:
        synth_props = synth.plugin.properties
        for prop in synth_props:
            p_name = prop.name
            p_type = prop.type
            p_val = prop.default_value
            prop_list.append(p_name)
            if p_type == "Integer":
                props_dict[p_name] = bpy.props.FloatProperty(default=p_val)
            elif p_type == "Float":
                props_dict[p_name] = bpy.props.FloatProperty(default=p_val)
            elif p_type == "Symbol":
                props_dict[p_name] = bpy.props.StringProperty(default=p_val)
            else:
                raise TypeError(f'Unsupported type ({p_type}) for {p_name}!')

            new_satie_prop = SatieSynthProperty(name=str(prop))
            new_satie_prop.property_dict = props_dict
        bpy.types.Object.SynthProps = props_dict
        new_satie_prop.generate_properties()


class CreateSynth(Operator):
    bl_idname = "satie.create"
    bl_label = "Create Synth, yo!"
    bl_description = "Create a synth instance"
    bl_options = {'INTERNAL'}

    def execute(self, context):
        satie = bpy.satie
        obj: bpy.object = context.object
        props: SatieObjectProperties = obj.satie
        synth: Plugin = satie.get_plugin_by_name(props.ui_synth)
        satie.create_source_node(obj.name, synth, props.group)
        bpy.satie_instances.append(obj)
        context.object.satie.enabled = True
        fill_properties(context)
        return {"FINISHED"}
