class Logger:
    """
    Simple grouping logger
    """

    indent = 0

    def __init__(self, target):
        """
        Create an instance of the logger
        :param target: Class to attach the logger to
        """

        self._name = target.__class__.__name__

    def open(self, *args, **kwargs):
        """
        Open a new log group, every log group needs to be closed
        :param args:
        :param kwargs:
        :return:
        """

        color = 31 + (len(self._name) % 6)  # 6 available colors + 1 because of modulo
        self._print('┌', '\033[1;' + str(color) + 'm', True, *args, **kwargs)
        Logger.indent += 1

    def log(self, *args, **kwargs):
        """
        Log a message in the currently opened group
        :param args:
        :param kwargs:
        :return:
        """

        self._print('', '\033[1;90m', True, *args, **kwargs)

    def _print(self, char, color, show_name, *args, **kwargs):
        """
        Internal printing method
        :param char: Opening character
        :param color: Label color
        :param show_name: Show label or not
        :param args:
        :param kwargs:
        :return:
        """

        print('\033[1;90m' + ("│   " * Logger.indent) + char + ' ' + ((color + self._name + ':') if show_name else '') + '\033[0m', *args, **kwargs, end='')
        print('\033[0m')

    def close(self, *args, **kwargs):
        """
        Close an opened group
        :param args:
        :param kwargs:
        :return:
        """

        Logger.indent -= 1
        if Logger.indent < 0:
            Logger.indent = 0
        self._print('└', '\033[1;96m', False, *args, **kwargs)
